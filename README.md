# yaml-to-ec2 (WIP)
A Python CLI tool to take in a YAML file in a provided format and create an EC2 instance using boto3 and PyYAML.

Currently, this tool only works with a config file in the same format as the provided 'example.yaml' file.
To use:
* Create a new config file or edit the existing `example.yaml` file and make sure it is in the root directory
* Run `python -m pip install -r requirements.txt` to install the dependencies
* Run `python yaml_to_ec2.py your_config.yaml`
 
This is still a work-in-progress and has not been tested on AWS, but included is a Docker Compose file to run localstack
 for local testing.