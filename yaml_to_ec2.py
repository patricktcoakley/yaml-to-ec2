#!/usr/bin/env python3

import boto3
import yaml
import os.path
import sys
import pprint


class TerminalColors:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    

def get_config(file_name):
    try:
        with open(file_name, 'r') as f:
            return yaml.load(f, Loader=yaml.FullLoader)
    except OSError as e:
        print(f'{TerminalColors.FAIL}Could not read file {file_name}: \n{e}')


def get_user_data(params):
    user_data = ['#!/bin/bash']
    print(f'{TerminalColors.HEADER}Creating instance with the following parameters:{TerminalColors.GREEN}')
    pprint.pprint(params)
    for volume in params['volumes']:
        if volume['mount'] != '/':
            user_data.append(f"sudo mkfs.{volume['type']} {volume['device']}")
            user_data.append(f"sudo mkdir {volume['mount']}")
            user_data.append(f"sudo mount {volume['device']} {volume['mount']}")
    for user in params['users']:
        user_data.append(f"sudo adduser {user['login']} --disabled-password")
        user_data.append(f"sudo su - {user['login']}")
        user_data.append("mkdir .ssh && chmod 700 .ssh && touch .ssh/authorized_keys && chmod 600 .ssh/authorized_keys")
        user_data.append(f"echo {user['ssh_key']} >> .ssh/authorized_keys")
        user_data.append('exit')
    return user_data


def create_ec2_instances(config):
    ec2 = boto3.resource('ec2')
    # Uncomment to use localstock included with Docker ec2 = boto3.resource('ec2', endpoint_url='http://localhost:4566')
    for server, params in config.items():
        if not os.path.exists('./keys'):
            os.mkdir('./keys')
        if not os.path.exists(f'./keys/{server}-ec2-keypair.pem'):
            out = open(f'./keys/{server}-ec2-keypair.pem', 'w')
            key_pair = ec2.create_key_pair(KeyName=f'{server}-ec2-keypair')
            out.write(str(key_pair.key_material))

        volumes = []
        user_data = get_user_data(params)
        for volume in params['volumes']:
            volumes.append({
                'DeviceName': volume['device'],
                'Ebs': {
                    'DeleteOnTermination': True,
                    'VolumeSize': volume['size_gb'],
                    'VolumeType': 'standard'},
            }
            )

        instance = ec2.create_instances(
            BlockDeviceMappings=volumes,
            ImageId=params['ami_type'],
            MinCount=params['min_count'],
            MaxCount=params['max_count'],
            InstanceType='t2.micro',
            KeyName=f'{server}-ec2-keypair',
            UserData="".join(user_data)
        )
        print(f'\n{TerminalColors.BLUE}Created instance: {instance[0].instance_id}')


def main():
    if len(sys.argv) != 2 or not sys.argv[1]:
        raise ValueError(f'{TerminalColors.FAIL}Invalid arguments. Usage: python yaml_to_ec2.py PATH')
    config = get_config(sys.argv[1])
    if not config:
        raise ValueError(f'{TerminalColors.FAIL}Invalid configuration.')
    create_ec2_instances(config)


if __name__ == "__main__":
    main()
